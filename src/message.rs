macro_rules! color {
    (blue) => ("\x1b[34m");
    (cyan) => ("\x1b[36m");
    (magenta) => ("\x1b[35m");
    (red) => ("\x1b[31m");
    (reset) => ("\x1b[0m");
    (yellow) => ("\x1b[33m");
}

// TODO: Use eprintln! (?)

macro_rules! error {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(red), "[ERROR] ", $format_str, color!(reset))
            $(,$args)*
        )
    };
}

macro_rules! info {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(blue), "[INFO] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! info_cache {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(magenta), "[CACHE] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! info_stats {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(cyan), "[STATS] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! info_transcoding {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(blue), "[TRANSCODING] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! warn {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(yellow), "[WARNING] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! warn_discouraged {
    ($format_str:expr $(,$args:expr)*) => {
        println!(
            concat!(color!(yellow), "[DISCOURAGED] ", $format_str, color!(reset))
            $(,$args)*
        );
    };
}

macro_rules! warn_global_set_repeatedly {
    ($key:expr) => {
        warn!(
            "Global {} is set more than once - overriding previous value",
            $key
        );
    };
    ($key:expr, $previous:expr, $new:expr) => {
        warn!(
            "Global {} is set more than once - overriding previous value '{}' with '{}'",
            $key,
            $previous,
            $new
        );
    };
}